#! /usr/bin/env python3

import sys
import os
import glob
import shutil

def printUsageAndExit() :
  print("USAGE : %s bin dest"%sys.argv[0], file=sys.stderr)
  exit(1)

if __name__ == "__main__" :
  if len(sys.argv) != 3 :
    printUsageAndExit()

  for filename in glob.iglob(sys.argv[1]+"/**", recursive=True) :
    if os.path.splitext(filename)[1] == ".rm" :
      shutil.copy(filename, sys.argv[2]+"/"+filename.split('/')[-2]+".rm")

