#! /usr/bin/env python3

import sys
from readMCD import readMCD

if len(sys.argv) < 3 :
  print("USAGE : %s FORMindex filename1 filename2..."%sys.argv[0])

baseMCD = sys.argv[1]

for filename in sys.argv[2:] :
  prefix = filename.split('/')[-1].split('.')[0]
  formIndex = int(sys.argv[1])
  lines = []
  for line in open(filename, "r") :
    lines.append(line.strip())
  with open(filename, "w") as out :
    for line in lines :
      if len(line) == 0 or line[0] == "#" :
        print(line, file=out)
        continue
      splited = line.split('\t')
      splited[formIndex] = prefix+"_"+splited[formIndex]
      print("\t".join(splited), file=out)
    
