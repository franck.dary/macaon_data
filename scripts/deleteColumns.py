#! /usr/bin/env python3

import sys

def printUsageAndExit() :
  print("USAGE : %s file.tsv column1 column2..."%sys.argv[0], file=sys.stderr)
  exit(1)

if __name__ == "__main__" :
  if len(sys.argv) < 2 :
    printUsageAndExit()

  for line in open(sys.argv[1], 'r') :
    if len(line) < 3 or line[0] == '#' :
      if len(line) >= 3 :
        print(line, end="")
      else :
        print("")
      continue

    splited = line.strip().split('\t')
    for i in sys.argv[2:] :
      col = int(i)
      if col in range(len(splited)) :
        splited[col] = "_"

    print('\t'.join(splited))

