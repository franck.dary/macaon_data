import os
import subprocess

def launchSlurmArray(names, commands, jobName, device, nbHours, limit, nbCPU) :
  commands = ["'%s'"%s for s in commands]
  names = ["'%s'"%s for s in names]

  filename = "{}.slurm".format(jobName)
  sFile = open(filename, "w")

  hostname = os.getenv("HOSTNAME")

  commandList = " ".join(commands)

  if ".cluster" in hostname :
    print("""#! /usr/bin/env bash

#SBATCH --array=0-{}%{}
#SBATCH --job-name={}
#SBATCH --output=%A_%a.out
#SBATCH --error=%A_%a.err
#SBATCH --open-mode=append
#SBATCH --ntasks=1
#SBATCH --cpus-per-task={}
#SBATCH --partition=skylake
#SBATCH -A b250
#SBATCH --time={}:00:00

module purge
module load userspace/all
module load gcc/10.2.0
module load python3/3.8.6

names=({})
commands=({})

newOut=${{names[$SLURM_ARRAY_TASK_ID]}}".stdout"
newErr=${{names[$SLURM_ARRAY_TASK_ID]}}".stderr"
oldOut=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".out"
oldErr=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".err"
tmpFile=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".tmp"

touch $newOut

cp $newOut $tmpFile
mv $oldOut $newOut
cat $tmpFile >> $newOut

touch $newErr

cp $newErr $tmpFile
mv $oldErr $newErr
cat $tmpFile >> $newErr

rm $tmpFile

>&2 echo "Job ID : " "$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID"
eval "${{commands[$SLURM_ARRAY_TASK_ID]}}"
""".format(len(names)-1, limit, jobName, nbCPU, nbHours, " ".join(names), " ".join(commands)), file=sFile)
    sFile.close()
  elif "jean-zay" in hostname :
    print("""#! /usr/bin/env bash

#SBATCH --array=0-{}%{}
#SBATCH --job-name={}
#SBATCH --output=%A_%a.out
#SBATCH --error=%A_%a.err
#SBATCH --open-mode=append
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=10
#SBATCH --gres=gpu:1
#SBATCH --hint=nomultithread
#SBATCH --qos={}
#SBATCH --time={}:00:00

module purge
module load gcc/9.1.0
module load python/3.7.5

names=({})
commands=({})

newOut=${{names[$SLURM_ARRAY_TASK_ID]}}".stdout"
newErr=${{names[$SLURM_ARRAY_TASK_ID]}}".stderr"
oldOut=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".out"
oldErr=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".err"
tmpFile=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".tmp"

touch $newOut

cp $newOut $tmpFile
mv $oldOut $newOut
cat $tmpFile >> $newOut

touch $newErr

cp $newErr $tmpFile
mv $oldErr $newErr
cat $tmpFile >> $newErr

rm $tmpFile

>&2 echo "Job ID : " "$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID"
eval "${{commands[$SLURM_ARRAY_TASK_ID]}}"
""".format(len(names)-1, limit, jobName, "qos_gpu-t4" if int(nbHours) > 20 else "qos_gpu-t3", nbHours, " ".join(names), " ".join(commands)), file=sFile)
    sFile.close()
  elif hostname == "sms-cluster.lis-lab.fr" :
    print('''#! /usr/bin/env bash

#SBATCH --array=0-{}%{}
#SBATCH --job-name={}
#SBATCH --output=%A_%a.out
#SBATCH --error=%A_%a.err
#SBATCH --open-mode=append
#SBATCH --ntasks=1
#SBATCH --cpus-per-task={}
#SBATCH --partition={}
#SBATCH --time={}:00:00

module purge

names=({})
commands=({})

newOut=${{names[$SLURM_ARRAY_TASK_ID]}}".stdout"
newErr=${{names[$SLURM_ARRAY_TASK_ID]}}".stderr"
oldOut=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".out"
oldErr=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".err"
tmpFile=$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID".tmp"

touch $newOut

cp $newOut $tmpFile
mv $oldOut $newOut
cat $tmpFile >> $newOut

touch $newErr

cp $newErr $tmpFile
mv $oldErr $newErr
cat $tmpFile >> $newErr

rm $tmpFile

>&2 echo "Job ID : " "$SLURM_ARRAY_JOB_ID"_"$SLURM_ARRAY_TASK_ID"
eval "${{commands[$SLURM_ARRAY_TASK_ID]}}"
'''.format(len(names)-1, limit, jobName, nbCPU, "cpu" if device == "cpu" else "all\n#SBATCH --gres=gpu", nbHours, " ".join(names), commandList), file=sFile)
    sFile.close()
  else :
    print("ERROR : Unknown hostname \'%s\'"%hostname)
    exit(1)

  subprocess.Popen("sbatch {}".format(filename), shell=True).wait()

