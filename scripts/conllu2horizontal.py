#! /usr/bin/env python3

import sys
from readMCD import readMCD

def isNumber(s) :
  hasDigit = False
  for c in s :
    if c.isdigit() :
      hasDigit = True
    if c.isalpha() :
      return False
  return hasDigit

def printUsageAndExit() :
  print("USAGE : %s file.conllu (columnName | LETTERS)"%sys.argv[0], file=sys.stderr)
  sys.exit(1)

if __name__ == "__main__" :
  if len(sys.argv) != 3 :
    printUsageAndExit()

  col2index, index2col = readMCD("ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL")
  col = sys.argv[2]

  if col != "LETTERS" :
    for line in open(sys.argv[1], "r") :
      if line.startswith("#") :
        splited = line.split("global.columns =")
        if len(splited) > 1 :
          col2index, index2col = readMCD(splited[-1].strip())
        continue

      if len(line.strip()) == 0 :
        print("")
        continue

      splited = line.strip().split("\t")

      if col not in col2index :
        print("ERROR : invalid columnName '%s'"%col)
        exit(1)
      index = col2index[col]
      if index not in range(len(splited)) :
        print("ERROR : column %s not found in line '%s'"%(index, line.strip()))
        exit(1)

      value = splited[index].replace(" ", "◌")
      if isNumber(splited[index].replace(" ", "").strip()) :
        value = 42
      print(value, end=" ")
  else :
    for line in open(sys.argv[1], "r") :
      if line.startswith("#") :
        splited = line.split("global.columns =")
        if len(splited) > 1 :
          col2index, index2col = readMCD(splited[-1].strip())
        splited = line.split("text =")
        if len(splited) > 1 :
          text = list(splited[-1].replace("\n", " ").replace(" ", "◌"))
          for elem in text :
            if isNumber(str(elem)) :
              elem = '0'
            print(elem, end=" ")
          print("")
