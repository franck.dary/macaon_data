#! /usr/bin/env python3

import sys

def printUsageAndExit() :
  print("USAGE : %s file.tsv"%sys.argv[0], file=sys.stderr)
  exit(1)

if __name__ == "__main__" :
  if len(sys.argv) != 2 :
    printUsageAndExit()

  curId = 1
  for line in open(sys.argv[1], 'r') :
    if len(line) < 3 or line[0] == '#' :
      curId = 1
      if len(line) >= 3 :
        print(line, end="")
      else :
        print("")
      continue

    splited = line.strip().split('\t')
    if curId == 1 :
      splited[6] = "0"
      splited[7] = "root"
    else :
      splited[6] = "1"

    print('\t'.join(splited))
    curId += 1

