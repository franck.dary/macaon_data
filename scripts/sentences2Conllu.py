#! /usr/bin/env python3

import sys
from readMCD import readMCD


col2index, index2col = readMCD("ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC")

print("# global.columns = %s"%(" ".join(col2index.keys())))

for line in open(sys.argv[1], "r") :
  line = line.strip()
  words = line.split()
  sentence = [["_" for _ in col2index] for _ in words]
  for i in range(len(sentence)) :
    sentence[i][col2index["ID"]] = str(i+1)
    sentence[i][col2index["FORM"]] = words[i]
    sentence[i][col2index["HEAD"]] = "0" if i == 0 else "1"
    sentence[i][col2index["DEPREL"]] = "root" if i == 0 else "_"
  print("# text = %s"%line)
  print("\n".join(["\t".join(word) for word in sentence]))
  print("")

