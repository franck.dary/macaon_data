#! /usr/bin/env bash

GLOVE="../../../../GloVe/"
HORIZONTAL="../../../../scripts/conllu2horizontal.py"

if [ "$#" -ne 4 ]; then
    echo "USAGE : $0 input.conllu colName embeddingsSize output.w2v"
    exit 1
fi

MINCOUNT=2
if [ $2 == "LETTERS" ]; then
	MINCOUNT=10
fi

cp -r $GLOVE .
GLOVE="GloVe/"
CURDIR="$(pwd)"
cd $GLOVE && make clean && make && cd $CURDIR \
&& $HORIZONTAL $1 $2 > in.text \
&& $GLOVE"build/vocab_count" -min-count $MINCOUNT < in.text > vocab.txt

[ -s vocab.txt ] \
&& $GLOVE"build/cooccur" -symmetric 0 -window-size 10 -vocab-file vocab.txt -memory 8.0 -overflow-file tempoverflow < in.text > cooccurrences.bin \
&& $GLOVE"build/shuffle" -memory 8.0 -seed 100 < cooccurrences.bin > cooccurrence.shuf.bin \
&& $GLOVE"build/glove" -iter 50 -save_gradsq 0 -write-header 1 -input-file cooccurrence.shuf.bin -vocab-file vocab.txt -save-file out -gradsq-file gradsq -vector-size $3 -seed 100 -threads 1 -alpha 0.75 -x-max 100.0 -eta 0.05 -binary 0 -model 1 \
&& cp out.txt $4

if [ ! -s $4 ] ; then
  echo "0 $3" > $4
fi

rm in.text 2> /dev/null
rm vocab.txt 2> /dev/null
rm cooccurrences.bin 2> /dev/null
rm cooccurrence.shuf.bin 2> /dev/null
rm overflow_*\.bin 2> /dev/null
rm gradsq.txt 2> /dev/null

exit 0
