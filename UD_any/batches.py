# To untrack the file do : git update-index --skip-worktree batches.py

templatesExperiments = [
  {
    'mode'      : 'txt',
    'expName'   : 'tokenizer',
    'template'  : 'tokenizer',
    'arguments' : '-n 2',
  },
]

langs = [
  "UD_French-Tiny",
#  "UD_French-GSD",
#  "UD_Hebrew-HTB",
#  "UD_Chinese-GSD",
#  "UD_English-EWT",
#  "UD_French-Spoken",
#  "UD_Russian-SynTagRus",
#  "UD_Arabic-PADT",
#  "UD_Finnish-TDT",
#  "UD_Turkish-IMST",
#  "UD_Norwegian-Bokmaal",
#  "UD_Romanian-RRT",
]

repRange = [0,1]

