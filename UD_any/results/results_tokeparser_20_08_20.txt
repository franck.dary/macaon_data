Corpus            Metric      F1.score        Model                           
--------------------------------------------------------------------------------
UD_French-GSD     LAS         81.84[±0.01]%   tokeparserIncrNone              
UD_French-GSD     LAS         82.41[±0.16]%   tokeparserIncrGloveFull         
UD_French-GSD     LAS         82.42[±0.16]%   tokeparserIncrFat               
UD_French-GSD     LAS         82.72%          tokeparserIncrGloveNew          
UD_French-GSD     LAS         83.16[±0.28]%   tokeparserIncrNew               
UD_French-GSD     LAS         83.34[±0.39]%   tokeparserIncrGlove             
UD_French-GSD     LAS         83.63[±0.23]%   tokeparserIncrPosFeatsLetters   
UD_French-GSD     LAS         83.72[±0.16]%   tokeparserIncr                  
UD_French-GSD     LAS         84.00[±0.05]%   tokeparserIncrPosFeats          

UD_French-GSD     Lemmas      97.18[±0.09]%   tokeparserIncrNone              
UD_French-GSD     Lemmas      97.45[±0.05]%   tokeparserIncrNew               
UD_French-GSD     Lemmas      97.50[±0.02]%   tokeparserIncrFat               
UD_French-GSD     Lemmas      97.50[±0.07]%   tokeparserIncr                  
UD_French-GSD     Lemmas      97.59[±0.06]%   tokeparserIncrPosFeatsLetters   
UD_French-GSD     Lemmas      97.60[±0.04]%   tokeparserIncrPosFeats          
UD_French-GSD     Lemmas      97.64[±0.10]%   tokeparserIncrGloveFull         
UD_French-GSD     Lemmas      97.71[±0.07]%   tokeparserIncrGlove             
UD_French-GSD     Lemmas      97.72%          tokeparserIncrGloveNew          

UD_French-GSD     Sentences   89.37[±0.77]%   tokeparserIncrPosFeats          
UD_French-GSD     Sentences   89.88[±0.21]%   tokeparserIncrNew               
UD_French-GSD     Sentences   90.03[±0.17]%   tokeparserIncrGloveFull         
UD_French-GSD     Sentences   90.64[±0.77]%   tokeparserIncrNone              
UD_French-GSD     Sentences   90.93[±0.46]%   tokeparserIncrFat               
UD_French-GSD     Sentences   91.15[±0.13]%   tokeparserIncr                  
UD_French-GSD     Sentences   91.19[±1.02]%   tokeparserIncrPosFeatsLetters   
UD_French-GSD     Sentences   91.22%          tokeparserIncrGloveNew          
UD_French-GSD     Sentences   91.22[±0.07]%   tokeparserIncrGlove             

UD_French-GSD     Tokens      99.29%          tokeparserIncrGloveNew          
UD_French-GSD     Tokens      99.29[±0.04]%   tokeparserIncrFat               
UD_French-GSD     Tokens      99.34[±0.05]%   tokeparserIncrPosFeatsLetters   
UD_French-GSD     Tokens      99.37[±0.02]%   tokeparserIncrNone              
UD_French-GSD     Tokens      99.38[±0.08]%   tokeparserIncrGloveFull         
UD_French-GSD     Tokens      99.39[±0.02]%   tokeparserIncrPosFeats          
UD_French-GSD     Tokens      99.41[±0.03]%   tokeparserIncrNew               
UD_French-GSD     Tokens      99.41[±0.04]%   tokeparserIncr                  
UD_French-GSD     Tokens      99.41[±0.07]%   tokeparserIncrGlove             

UD_French-GSD     UAS         84.72[±0.02]%   tokeparserIncrNone              
UD_French-GSD     UAS         85.21[±0.09]%   tokeparserIncrFat               
UD_French-GSD     UAS         85.31[±0.22]%   tokeparserIncrGloveFull         
UD_French-GSD     UAS         85.53%          tokeparserIncrGloveNew          
UD_French-GSD     UAS         85.76[±0.08]%   tokeparserIncrNew               
UD_French-GSD     UAS         86.13[±0.32]%   tokeparserIncrGlove             
UD_French-GSD     UAS         86.30[±0.14]%   tokeparserIncrPosFeatsLetters   
UD_French-GSD     UAS         86.50[±0.03]%   tokeparserIncr                  
UD_French-GSD     UAS         86.75[±0.04]%   tokeparserIncrPosFeats          

UD_French-GSD     UFeats      94.95[±0.14]%   tokeparserIncrFat               
UD_French-GSD     UFeats      94.96[±0.01]%   tokeparserIncrNone              
UD_French-GSD     UFeats      95.03%          tokeparserIncrGloveNew          
UD_French-GSD     UFeats      95.21[±0.15]%   tokeparserIncrPosFeatsLetters   
UD_French-GSD     UFeats      95.31%          tokeparserIncrGloveFull         
UD_French-GSD     UFeats      95.37[±0.07]%   tokeparserIncrPosFeats          
UD_French-GSD     UFeats      95.38[±0.07]%   tokeparserIncrNew               
UD_French-GSD     UFeats      95.40[±0.02]%   tokeparserIncrGlove             
UD_French-GSD     UFeats      95.45[±0.04]%   tokeparserIncr                  

UD_French-GSD     UPOS        96.25[±0.02]%   tokeparserIncrNone              
UD_French-GSD     UPOS        96.67[±0.02]%   tokeparserIncrFat               
UD_French-GSD     UPOS        96.69[±0.11]%   tokeparserIncrGloveFull         
UD_French-GSD     UPOS        96.72[±0.12]%   tokeparserIncr                  
UD_French-GSD     UPOS        96.74%          tokeparserIncrGloveNew          
UD_French-GSD     UPOS        96.77[±0.08]%   tokeparserIncrNew               
UD_French-GSD     UPOS        96.78[±0.18]%   tokeparserIncrPosFeatsLetters   
UD_French-GSD     UPOS        96.81[±0.02]%   tokeparserIncrPosFeats          
UD_French-GSD     UPOS        96.87[±0.07]%   tokeparserIncrGlove             

UD_French-GSD     Words       99.12[±0.06]%   tokeparserIncrFat               
UD_French-GSD     Words       99.14%          tokeparserIncrGloveNew          
UD_French-GSD     Words       99.16[±0.09]%   tokeparserIncrGloveFull         
UD_French-GSD     Words       99.17[±0.03]%   tokeparserIncrPosFeatsLetters   
UD_French-GSD     Words       99.21[±0.05]%   tokeparserIncrNone              
UD_French-GSD     Words       99.22[±0.03]%   tokeparserIncrPosFeats          
UD_French-GSD     Words       99.22[±0.04]%   tokeparserIncrGlove             
UD_French-GSD     Words       99.23[±0.04]%   tokeparserIncr                  
UD_French-GSD     Words       99.25[±0.04]%   tokeparserIncrNew               
--------------------------------------------------------------------------------
UD_Turkish-IMST   LAS         49.48[±0.62]%   tokeparserIncrNone              
UD_Turkish-IMST   LAS         51.74[±0.58]%   tokeparserIncrFat               
UD_Turkish-IMST   LAS         52.93[±0.14]%   tokeparserIncrPosFeats          
UD_Turkish-IMST   LAS         52.97[±0.30]%   tokeparserIncrPosFeatsLetters   
UD_Turkish-IMST   LAS         53.03[±0.18]%   tokeparserIncrNew               
UD_Turkish-IMST   LAS         53.64[±0.11]%   tokeparserIncr                  
UD_Turkish-IMST   LAS         53.64[±0.36]%   tokeparserIncrGloveFull         
UD_Turkish-IMST   LAS         54.17[±0.07]%   tokeparserIncrGloveNew          
UD_Turkish-IMST   LAS         54.49[±0.45]%   tokeparserIncrGlove             

UD_Turkish-IMST   Lemmas      86.62[±0.19]%   tokeparserIncrNone              
UD_Turkish-IMST   Lemmas      88.17[±0.14]%   tokeparserIncrFat               
UD_Turkish-IMST   Lemmas      88.24[±0.11]%   tokeparserIncrPosFeatsLetters   
UD_Turkish-IMST   Lemmas      88.45[±0.08]%   tokeparserIncrNew               
UD_Turkish-IMST   Lemmas      88.72[±0.17]%   tokeparserIncrPosFeats          
UD_Turkish-IMST   Lemmas      88.73[±0.20]%   tokeparserIncr                  
UD_Turkish-IMST   Lemmas      89.99[±0.15]%   tokeparserIncrGloveFull         
UD_Turkish-IMST   Lemmas      90.44[±0.00]%   tokeparserIncrGloveNew          
UD_Turkish-IMST   Lemmas      90.77[±0.05]%   tokeparserIncrGlove             

UD_Turkish-IMST   Sentences   94.87[±0.23]%   tokeparserIncrNone              
UD_Turkish-IMST   Sentences   95.40[±0.13]%   tokeparserIncrGlove             
UD_Turkish-IMST   Sentences   95.44[±0.28]%   tokeparserIncrGloveNew          
UD_Turkish-IMST   Sentences   95.56[±0.24]%   tokeparserIncrGloveFull         
UD_Turkish-IMST   Sentences   95.75[±0.16]%   tokeparserIncrFat               
UD_Turkish-IMST   Sentences   95.76[±0.20]%   tokeparserIncrPosFeatsLetters   
UD_Turkish-IMST   Sentences   95.87[±0.51]%   tokeparserIncrNew               
UD_Turkish-IMST   Sentences   95.94[±0.58]%   tokeparserIncrPosFeats          
UD_Turkish-IMST   Sentences   96.22[±0.05]%   tokeparserIncr                  

UD_Turkish-IMST   Tokens      99.37[±0.02]%   tokeparserIncrNone              
UD_Turkish-IMST   Tokens      99.40[±0.05]%   tokeparserIncrGloveFull         
UD_Turkish-IMST   Tokens      99.47[±0.02]%   tokeparserIncrGlove             
UD_Turkish-IMST   Tokens      99.47[±0.02]%   tokeparserIncrGloveNew          
UD_Turkish-IMST   Tokens      99.51[±0.05]%   tokeparserIncrNew               
UD_Turkish-IMST   Tokens      99.53[±0.01]%   tokeparserIncrPosFeatsLetters   
UD_Turkish-IMST   Tokens      99.53[±0.02]%   tokeparserIncrPosFeats          
UD_Turkish-IMST   Tokens      99.53[±0.03]%   tokeparserIncrFat               
UD_Turkish-IMST   Tokens      99.54%          tokeparserIncr                  

UD_Turkish-IMST   UAS         58.35[±0.68]%   tokeparserIncrNone              
UD_Turkish-IMST   UAS         59.76[±0.69]%   tokeparserIncrFat               
UD_Turkish-IMST   UAS         60.83[±0.03]%   tokeparserIncrPosFeats          
UD_Turkish-IMST   UAS         61.06[±0.25]%   tokeparserIncrGloveFull         
UD_Turkish-IMST   UAS         61.08[±0.14]%   tokeparserIncrNew               
UD_Turkish-IMST   UAS         61.12[±0.43]%   tokeparserIncrPosFeatsLetters   
UD_Turkish-IMST   UAS         61.42[±0.21]%   tokeparserIncr                  
UD_Turkish-IMST   UAS         61.56[±0.11]%   tokeparserIncrGloveNew          
UD_Turkish-IMST   UAS         61.83[±0.35]%   tokeparserIncrGlove             

UD_Turkish-IMST   UFeats      83.33[±0.77]%   tokeparserIncrNone              
UD_Turkish-IMST   UFeats      85.37[±0.29]%   tokeparserIncrFat               
UD_Turkish-IMST   UFeats      85.77[±0.08]%   tokeparserIncrPosFeatsLetters   
UD_Turkish-IMST   UFeats      86.16[±0.16]%   tokeparserIncrNew               
UD_Turkish-IMST   UFeats      86.18[±0.08]%   tokeparserIncr                  
UD_Turkish-IMST   UFeats      86.28[±0.01]%   tokeparserIncrPosFeats          
UD_Turkish-IMST   UFeats      87.57[±0.03]%   tokeparserIncrGloveFull         
UD_Turkish-IMST   UFeats      88.34[±0.09]%   tokeparserIncrGloveNew          
UD_Turkish-IMST   UFeats      88.77[±0.02]%   tokeparserIncrGlove             

UD_Turkish-IMST   UPOS        87.01[±0.69]%   tokeparserIncrNone              
UD_Turkish-IMST   UPOS        90.05%          tokeparserIncrPosFeatsLetters   
UD_Turkish-IMST   UPOS        90.35[±0.12]%   tokeparserIncrFat               
UD_Turkish-IMST   UPOS        90.56[±0.41]%   tokeparserIncrNew               
UD_Turkish-IMST   UPOS        90.94[±0.15]%   tokeparserIncrPosFeats          
UD_Turkish-IMST   UPOS        91.02[±0.16]%   tokeparserIncr                  
UD_Turkish-IMST   UPOS        92.45[±0.11]%   tokeparserIncrGloveFull         
UD_Turkish-IMST   UPOS        93.03[±0.03]%   tokeparserIncrGloveNew          
UD_Turkish-IMST   UPOS        93.21[±0.12]%   tokeparserIncrGlove             

UD_Turkish-IMST   Words       97.25[±0.01]%   tokeparserIncrNone              
UD_Turkish-IMST   Words       97.31[±0.07]%   tokeparserIncrGloveFull         
UD_Turkish-IMST   Words       97.40[±0.02]%   tokeparserIncrGlove             
UD_Turkish-IMST   Words       97.41[±0.03]%   tokeparserIncrGloveNew          
UD_Turkish-IMST   Words       97.43[±0.01]%   tokeparserIncr                  
UD_Turkish-IMST   Words       97.43[±0.01]%   tokeparserIncrPosFeatsLetters   
UD_Turkish-IMST   Words       97.44[±0.00]%   tokeparserIncrPosFeats          
UD_Turkish-IMST   Words       97.44[±0.02]%   tokeparserIncrFat               
UD_Turkish-IMST   Words       97.44[±0.04]%   tokeparserIncrNew               
