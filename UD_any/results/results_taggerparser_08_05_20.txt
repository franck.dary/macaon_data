Corpus            Metric      F1.score   Model               
--------------------------------------------------------------------------------
UD_English-EWT    LAS         71.68%     taggerparser_incr   
UD_English-EWT    LAS         72.94%     taggerparser_base   
UD_English-EWT    LAS         73.08%     taggerparser_seq    

UD_English-EWT    Sentences   70.87%     taggerparser_seq    
UD_English-EWT    Sentences   71.03%     taggerparser_incr   
UD_English-EWT    Sentences   71.09%     taggerparser_base   

UD_English-EWT    UAS         76.41%     taggerparser_incr   
UD_English-EWT    UAS         77.37%     taggerparser_base   
UD_English-EWT    UAS         77.73%     taggerparser_seq    

UD_English-EWT    UPOS        91.50%     taggerparser_incr   
UD_English-EWT    UPOS        91.90%     taggerparser_seq    
UD_English-EWT    UPOS        92.28%     taggerparser_base   
--------------------------------------------------------------------------------
UD_French-GSD     LAS         83.30%     taggerparser_incr   
UD_French-GSD     LAS         83.31%     taggerparser_seq    
UD_French-GSD     LAS         83.38%     taggerparser_base   

UD_French-GSD     Sentences   92.18%     taggerparser_seq    
UD_French-GSD     Sentences   93.14%     taggerparser_incr   
UD_French-GSD     Sentences   94.36%     taggerparser_base   

UD_French-GSD     UAS         86.60%     taggerparser_seq    
UD_French-GSD     UAS         86.72%     taggerparser_base   
UD_French-GSD     UAS         86.78%     taggerparser_incr   

UD_French-GSD     UPOS        96.26%     taggerparser_incr   
UD_French-GSD     UPOS        96.46%     taggerparser_seq    
UD_French-GSD     UPOS        96.47%     taggerparser_base   
--------------------------------------------------------------------------------
UD_Romanian-RRT   LAS         73.57%     taggerparser_base   
UD_Romanian-RRT   LAS         73.82%     taggerparser_incr   
UD_Romanian-RRT   LAS         73.82%     taggerparser_seq    

UD_Romanian-RRT   Sentences   93.98%     taggerparser_seq    
UD_Romanian-RRT   Sentences   95.08%     taggerparser_base   
UD_Romanian-RRT   Sentences   95.57%     taggerparser_incr   

UD_Romanian-RRT   UAS         81.01%     taggerparser_incr   
UD_Romanian-RRT   UAS         81.02%     taggerparser_base   
UD_Romanian-RRT   UAS         81.18%     taggerparser_seq    

UD_Romanian-RRT   UPOS        94.19%     taggerparser_incr   
UD_Romanian-RRT   UPOS        94.60%     taggerparser_seq    
UD_Romanian-RRT   UPOS        95.04%     taggerparser_base   
