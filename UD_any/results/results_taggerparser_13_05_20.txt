Corpus            Metric      F1.score        Model               
--------------------------------------------------------------------------------
UD_English-EWT    LAS         71.36[±0.57]%   taggerparser_incr   
UD_English-EWT    LAS         72.14[±0.58]%   taggerparser_base   
UD_English-EWT    LAS         72.64[±0.82]%   taggerparser_seq    

UD_English-EWT    Sentences   72.31[±0.76]%   taggerparser_incr   
UD_English-EWT    Sentences   73.06[±1.10]%   taggerparser_base   
UD_English-EWT    Sentences   73.29[±0.91]%   taggerparser_seq    

UD_English-EWT    UAS         76.34[±0.66]%   taggerparser_incr   
UD_English-EWT    UAS         76.64[±0.57]%   taggerparser_base   
UD_English-EWT    UAS         77.27[±0.70]%   taggerparser_seq    

UD_English-EWT    UPOS        90.86[±0.28]%   taggerparser_incr   
UD_English-EWT    UPOS        92.03[±0.40]%   taggerparser_seq    
UD_English-EWT    UPOS        92.18[±0.24]%   taggerparser_base   
--------------------------------------------------------------------------------
UD_French-GSD     LAS         83.43[±0.27]%   taggerparser_incr   
UD_French-GSD     LAS         83.73[±0.24]%   taggerparser_seq    
UD_French-GSD     LAS         83.81[±0.08]%   taggerparser_base   

UD_French-GSD     Sentences   93.54[±0.31]%   taggerparser_base   
UD_French-GSD     Sentences   93.75[±0.43]%   taggerparser_incr   
UD_French-GSD     Sentences   93.76[±0.48]%   taggerparser_seq    

UD_French-GSD     UAS         86.76[±0.30]%   taggerparser_incr   
UD_French-GSD     UAS         86.90[±0.38]%   taggerparser_seq    
UD_French-GSD     UAS         87.04[±0.01]%   taggerparser_base   

UD_French-GSD     UPOS        96.24[±0.09]%   taggerparser_incr   
UD_French-GSD     UPOS        96.62[±0.14]%   taggerparser_seq    
UD_French-GSD     UPOS        96.70[±0.02]%   taggerparser_base   
--------------------------------------------------------------------------------
UD_Romanian-RRT   LAS         75.00[±0.39]%   taggerparser_incr   
UD_Romanian-RRT   LAS         75.90[±0.59]%   taggerparser_seq    
UD_Romanian-RRT   LAS         76.04[±0.49]%   taggerparser_base   

UD_Romanian-RRT   Sentences   94.97[±0.36]%   taggerparser_base   
UD_Romanian-RRT   Sentences   95.65[±0.08]%   taggerparser_incr   
UD_Romanian-RRT   Sentences   95.77[±0.25]%   taggerparser_seq    

UD_Romanian-RRT   UAS         81.67[±0.27]%   taggerparser_incr   
UD_Romanian-RRT   UAS         82.50[±0.44]%   taggerparser_seq    
UD_Romanian-RRT   UAS         82.58[±0.51]%   taggerparser_base   

UD_Romanian-RRT   UPOS        94.77[±0.31]%   taggerparser_incr   
UD_Romanian-RRT   UPOS        95.51[±0.31]%   taggerparser_seq    
UD_Romanian-RRT   UPOS        95.68[±0.23]%   taggerparser_base   
