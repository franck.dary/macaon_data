Corpus            Metric      F1.score   Model            
--------------------------------------------------------------------------------
UD_English-EWT    LAS         70.89%     tagparser_incr   
UD_English-EWT    LAS         73.25%     tagparser_seq    
UD_English-EWT    LAS         73.51%     tagparser_base   

UD_English-EWT    Sentences   72.38%     tagparser_incr   
UD_English-EWT    Sentences   72.82%     tagparser_base   
UD_English-EWT    Sentences   72.82%     tagparser_seq    

UD_English-EWT    UAS         75.69%     tagparser_incr   
UD_English-EWT    UAS         77.40%     tagparser_seq    
UD_English-EWT    UAS         77.65%     tagparser_base   

UD_English-EWT    UFeats      93.10%     tagparser_incr   
UD_English-EWT    UFeats      94.07%     tagparser_base   
UD_English-EWT    UFeats      94.57%     tagparser_seq    

UD_English-EWT    UPOS        91.42%     tagparser_incr   
UD_English-EWT    UPOS        92.85%     tagparser_base   
UD_English-EWT    UPOS        93.09%     tagparser_seq    
--------------------------------------------------------------------------------
UD_French-GSD     LAS         83.57%     tagparser_seq    
UD_French-GSD     LAS         83.68%     tagparser_incr   
UD_French-GSD     LAS         83.93%     tagparser_base   

UD_French-GSD     Sentences   93.56%     tagparser_base   
UD_French-GSD     Sentences   94.09%     tagparser_seq    
UD_French-GSD     Sentences   94.70%     tagparser_incr   

UD_French-GSD     UAS         87.09%     tagparser_seq    
UD_French-GSD     UAS         87.10%     tagparser_incr   
UD_French-GSD     UAS         87.20%     tagparser_base   

UD_French-GSD     UFeats      95.82%     tagparser_incr   
UD_French-GSD     UFeats      95.84%     tagparser_base   
UD_French-GSD     UFeats      96.10%     tagparser_seq    

UD_French-GSD     UPOS        96.51%     tagparser_incr   
UD_French-GSD     UPOS        96.80%     tagparser_seq    
UD_French-GSD     UPOS        96.99%     tagparser_base   
--------------------------------------------------------------------------------
UD_Romanian-RRT   LAS         76.38%     tagparser_incr   
UD_Romanian-RRT   LAS         76.79%     tagparser_seq    
UD_Romanian-RRT   LAS         76.94%     tagparser_base   

UD_Romanian-RRT   Sentences   94.87%     tagparser_incr   
UD_Romanian-RRT   Sentences   95.08%     tagparser_base   
UD_Romanian-RRT   Sentences   95.20%     tagparser_seq    

UD_Romanian-RRT   UAS         82.87%     tagparser_seq    
UD_Romanian-RRT   UAS         82.92%     tagparser_incr   
UD_Romanian-RRT   UAS         83.18%     tagparser_base   

UD_Romanian-RRT   UFeats      93.91%     tagparser_incr   
UD_Romanian-RRT   UFeats      94.02%     tagparser_seq    
UD_Romanian-RRT   UFeats      94.52%     tagparser_base   

UD_Romanian-RRT   UPOS        95.45%     tagparser_incr   
UD_Romanian-RRT   UPOS        95.85%     tagparser_seq    
UD_Romanian-RRT   UPOS        96.15%     tagparser_base   
